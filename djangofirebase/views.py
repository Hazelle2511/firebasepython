from datetime import date
import pyrebase 
from django.shortcuts import render 
from django.contrib import auth

config = {

    "apiKey": "AIzaSyDHCLSRcROIOJuAJx8YJ4cIBDbHh6IUoQo",

    "authDomain": "fir-python-d3065.firebaseapp.com",

    "databaseURL": "https://fir-python-d3065-default-rtdb.europe-west1.firebasedatabase.app",


    "projectId": "fir-python-d3065",

    "storageBucket": "fir-python-d3065.appspot.com",

    "messagingSenderId": "72490244377",

    "appId": "1:72490244377:web:2bf090851c7f650fa30d88"

}


firebase = pyrebase.initialize_app(config)
authe = firebase.auth()
db = firebase.database()
# datas = {"message": "cleaning the house"}
# db.child("users").child("messages").push(datas)
# data = {"message": "hello"}
# db.child("users").child("Hello").child("Messages").set(data)

def signIn(request):
    return render(request, "signIn.html")

def postSign(request):

    #authentification
    email = request.POST.get("email")
    passw = request.POST.get("pass")

    try:
        user = authe.sign_in_with_email_and_password(email,passw)
    except:
        msg = "Your email or password is incorrect"
        return render(request,"signIn.html",{"msg":msg})
    # print(user['idToken'])
    # saving user to session
    # request.session["sess_user"] = user
    session_id = user['idToken']
    request.session['uid']=str(session_id)

    # retrieve messages from Firebase
    # messages = getFirebaseMessages()

    #my code
    to_do = db.child('to_do').child('name').get().val()

    return render(request, "welcome.html",  {"e":user["email"], "to_do":to_do})

# mycode
def postMessage(request):
    import time
    from datetime import datetime
    time_now = datetime.now()
    millis = int(time.mktime(time_now.timetuple()))
    print("Millis"+str(millis))
    message = request.POST.get('message')
    idtoken=request.session['uid']
    a = authe.get_account_info(idtoken)
    a = a['users']
    a = a[0]
    a = a['localId']
    print("INFO"+str(a))

    data = {
        "message": message
    }
    # db.child('users').child(a).set(data)
    db.child('users').child(a).child('to-do').child(millis).set(data)

    # task = db.child('users').child(a).child('message').get().val()
    task = db.child('users').child(a).child('to-do').child(millis).child('message').get().val()

    return render(request, "welcome.html",{"to_do":task})



# def postMessage(request):
#     data = {
#         "message": request.POST.get("message")
#     }

#     # retrieve user from session
#     user = request.session["sess_user"]

#     # refresh token
#     auth.refresh(user['refreshToken'])

#     # Pass the user's idToken to the push method
#     results = db.child("messages").push(data, user["idToken"])

#     # retrieve messages from Firebase
#     messages = getFirebaseMessages()

#     return render(request, "welcome.html", {"e":user["email"],"m":messages})



# def getFirebaseMessages():
#     """
#     Because Pyrebase generate a list of objects, and each
#     object must be called with .val(), we can not pass it directly
#     to the views (because the functions no more exists).
#     So we must build a simple list of values...
#     """
#     list_messages = {}
#     messages = db.child("messages").get()

#     for m in messages.each():
#         list_messages[m.key()] = m.val()["message"]

#     return list_messages

def logout(request):
      auth.logout(request)
      return render(request,'signIn.html')